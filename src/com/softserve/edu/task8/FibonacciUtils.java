package com.softserve.edu.task8;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class allows user to get Fibonacci number of specified position
 * in series. Also allows to get all Fibonacci numbers in specified range.
 * @author Danil Zhyliaiev
 *
 */
public final class FibonacciUtils {
    /**
     * {@code HashMap} used to store calculated
     * pairs <position in series - fibonacci number>. Used
     * to speed up getFib() method with big numbers.
     */
    private static final HashMap<Integer, Long> FIB_SERIES = new HashMap<>();

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private FibonacciUtils() { }

    /**
     * Gets a value from Fibonacci series at desired position.
     * @param number    desired position in Fibonacci series as {@code int}.
     * @return          Fibonacci number as {@code long}.
     */
    public static long getFib(final int number) {
        if (number == 0) {
            return 0;
        } else if (number == 1) {
            return 1;
        } else if (FIB_SERIES.containsKey(number)) {
            return FIB_SERIES.get(number);
        }

        long left = getFib(number - 1);
        long right = getFib(number - 2);
        long fib;

        // Prevent long overflow;
        if (right > (Long.MAX_VALUE - left)) {
            throw new ArithmeticException("Long Overflow");
        } else {
            fib = left + right;
        }

        FIB_SERIES.put(number, fib);

        return fib;
    }

    /**
     * Gets {@code ArrayList} with all Fibonacci numbers in specified range.
     * @param bot   desired bottom limit(inclusive) as {@code long}.
     * @param top   desired top limit(exclusive) as {@code long}.
     * @return      list of Fibonacci numbers as {@code ArrayList}.
     */
    public static ArrayList<Long> getFibInRange(final long bot, final long top) {
        // If bot > top - throw IllegalArgumentException.
        if (bot > top) {
            throw new IllegalArgumentException("Bottom limit [" + bot + "] "
                    + "cannot be greater than top limit [" + top + "].");
        } else  if (bot < 0 || top < 0) {
            throw  new IllegalArgumentException("Negative range is not supported. "
                    + "Only positive integers are allowed");
        }

        ArrayList<Long> fibSequence = new ArrayList<>();
        int iterator = 0;
        long fib = getFib(iterator);

        while (fib < top) {
            if (fib >= bot) {
                fibSequence.add(fib);
            }

            try {
                fib = getFib(++iterator);
            } catch (ArithmeticException e) {
                return fibSequence;
            }
        }

        return fibSequence;
    }
}
