package com.softserve.edu.task8;

/**
 * Class-storage for default application messages.
 * @author Danil Zhyliaiev
 */
public final class Messages {
    /** Message used before shut down the program. Used to provide help to user. */
    public static final String HELP_MESSAGE = "How to use:\n"
            + "Program prints all Fibonacci numbers in specified range.\n"
            + "Run program with 2 positive integers as arguments using format:\n"
            + "<bottom limit(inclusive)> <top limit(exclusive)>\n"
            + "- Only positive integers are allowed.\n"
            + "- Current maximum limit value: 2^63-1 (" + Long.MAX_VALUE + ").\n"
            + "- Program prints numbers in ascending order devided by comma.\n"
            + "- To view this help run program without arguments.";
    /** Message used before shut down program. Used to inform user how to get help. */
    public static final String HOW_TO_GET_HELP = "Run program with no arguments "
            + "to get a detailed guide.";
    /** Message to display if unsupported number of arguments were given. */
    public static final String WRONG_NUMBER_FORMAT = "Wrong number format. "
            + "Only positive integers are accepted.";
    /** Wrong number of arguments error message. */
    public static final String ARGUMENTS_NUMBER_ERROR = "Wrong number of arguments. "
            + "Only allowed format is:\n"
            + "<bottom limit(inclusive)> <top limit(exclusive)>";

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private Messages() { }
}
