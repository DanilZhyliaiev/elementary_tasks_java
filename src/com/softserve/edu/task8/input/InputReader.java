package com.softserve.edu.task8.input;

import com.softserve.edu.task8.Messages;

/**
 * Class to handle input needed for application.
 * @author Danil Zhyliaiev
 */
public class InputReader {
    /**
     * Verify given arguments to meet app requirements.
     * If number of arguments is invalid - print message and exit.
     * @param args  a {@code String[]} containing Command Line arguments
     *              to be verified.
     */
    private void verifyArgs(final String[] args) {
        if (args.length == 0) {
            System.out.println(Messages.HELP_MESSAGE);

            System.exit(0);
        } else if (args.length != 2) {
            System.out.println(Messages.ARGUMENTS_NUMBER_ERROR);
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }
    }

    /**
     * Parses arguments to {@code long} and return this values as {@code long[]}.
     * @param args          CL arguments.
     * @return              Range representet as {@code long[]}.
     */
    public final long[] readRange(final String[] args) {
        verifyArgs(args);

        long[] range = new long[2];
        // Try to parse limits from args. If can't - print message and exit.
        try {
            range[0] = Long.parseLong(args[0]);
            range[1] = Long.parseLong(args[1]);
        } catch (NumberFormatException e) {
            System.out.println(Messages.WRONG_NUMBER_FORMAT);
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }

        return range;
    }
}
