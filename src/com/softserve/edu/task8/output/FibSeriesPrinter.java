package com.softserve.edu.task8.output;

import java.util.Iterator;
import java.util.List;

/**
 * Class to handle fibonacci series output.
 * @author Danil Zhyliaiev
 */
public class FibSeriesPrinter {
    /**
     * Prints given list of fibonacci numbers.
     * If list is empty - prints 'NONE'.
     * @param range             Search range as {@code long[]}.
     * @param fibonacciSeries   list of fibonacci numbers.
     */

    public final void printFibonacciList(final long[] range, final List<Long> fibonacciSeries) {
        long botLimit = range[0];
        long topLimit = range[1];

        System.out.println("Fibonacci numbers in range [" + botLimit + ", "
                + topLimit + "]:");

        // If no fibonacci numbers were found in range print 'NONE', otherwise print numbers
        if (fibonacciSeries.size() == 0) {
            System.out.print("NONE");
            if (botLimit < 0) {
                System.out.println("(application doesn't count negative fibonacci numbers)");
            }
            System.out.println();
        } else {
            for (Iterator<Long> iterator = fibonacciSeries.iterator();
                 iterator.hasNext();) {
                Long fib = iterator.next();
                System.out.print(fib);
                if (iterator.hasNext()) {
                    System.out.print(", ");
                }
            }
        }

    }
}
