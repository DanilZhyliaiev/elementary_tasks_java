package com.softserve.edu.task8;

import com.softserve.edu.task8.input.InputReader;
import com.softserve.edu.task8.output.FibSeriesPrinter;

import java.util.ArrayList;

/**
 * This app reads arguments to get range.
 * Prints all fibonacci numbers in that range.
 * @author Danil Zhyliaiev
 *
 */
public final class App {
    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private App() { }

    /**
     * Task8 entry point.
     * @param args
     *      Parameters From Command Line.
     */
    public static void main(final String[] args) {
        InputReader inputReader = new InputReader();

        long[] range = inputReader.readRange(args);

        ArrayList<Long> fibs = null;
        try {
            fibs = FibonacciUtils.getFibInRange(range[0], range[1]);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }

        new FibSeriesPrinter().printFibonacciList(range, fibs);
    }
}
