package com.softserve.edu.task6;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * This app reads file to get mode. Count amount of lucky tickets
 * according to given mode.
 * @author Danil Zhyliaiev
 *
 */
public final class App {
    /** Message used before shut down program. Used to provide help to user. */
    private static final String HELP_MESSAGE = "How to use:\n"
            + "Program count the number of all possible lucky tickets for desired technique.\n"
            + "Currently supported tecniques are:\n"
            + "    1.'Moscow'   - the ticket is lucky if sum of its first three numbers equal to sum of second three.\n"
            + "    2.'Piter'    - the ticket is lucky if sum of its odd numbers equal to sum of even.\n"
            + "Program takes full filepath as argument. The first letters in the file determine mode.\n"
            + "Program is case sensitive.\n"
            + "To view this help run program without arguments.";
    /** Message used before shut down program. Used to inform user how to get help. */
    private static final String HOW_TO_GET_HELP = "Run program with no arguments "
            + "to get a detailed guide.";

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private App() { }

    /**
     * Task6 entry point.
     * @param args
     *      Parameters From Command Line.
     * @throws IOException .
     */
    public static void main(final String[] args) throws IOException {
        // If number of arguments is invalid - print message and exit.
        verifyArgs(args);

        String mode = getMode(args[0]);
        System.out.println("The number of all possible lucky tickets using '"
                + mode + "' technique: " + TicketUtils.countUpAllLucky(mode));

        // Uncomment next lines to print all lucky numbers.
//        System.out.println();
//        bonus();
    }

    /**
     * Verify given arguments to meet app requirements.
     * @param args  a {@code String[]} containing Command Line arguments
     *              to be verified.
     */
    private static void verifyArgs(final String[] args) {
        if (args.length == 0) {
            System.out.println(HELP_MESSAGE);

            System.exit(1);
        } else if (args.length != 1) {
            System.out.println("Wrong number of arguments.");
            System.out.println(HOW_TO_GET_HELP);

            System.exit(1);
        }
    }

    /**
     * Reads mode from file.
     * @param path          path of the file containing mode.
     * @return              {@code String "Moskow"} if mode is 'Moskow'.
     *                      {@code String "Piter"} if mode is 'Piter'.
     * @throws IOException  .
     */
    private static String getMode(final String path) throws IOException {
       File file = new File(path);

       // checks if file exist and is a file. If not - print message and exit
       if (!file.isFile()) {
           System.out.println("File '" + path + "' doesn't exist "
                   + "or is a directory.");
           System.out.println(HOW_TO_GET_HELP);

           System.exit(1);
       }

       // Reads first string in file and trim spaces from it
       Scanner scanner = new Scanner(file);
       String mode = scanner.next();
       mode = mode.trim();

       if (mode.startsWith("Moskow")) {
           mode = "Moskow";
       } else if (mode.startsWith("Piter")) {
           mode = "Piter";
       } else {
           System.out.println("Wrong mode. File '" + path + "' Has "
                   + "no appropriate keyword.");
           System.out.println(HOW_TO_GET_HELP);
           System.exit(1);
       }

       scanner.close();
       return mode;
   }
}

