package com.softserve.edu.task6;

import java.util.HashSet;

/**
 * An abstract representation of a 6-digit ticket.
 * @author Danil Zhyliaiev
 *
 */
public class Ticket {
    /** list of all possible lucky 6-digit tickets(both techniques). */
    private static HashSet<Ticket> allTickets = new HashSet<>();
    /** Ticket number. */
    private String number;
    /** Number of digits in ticket number. */
    public static final int NUMBER_OF_DIGITS = 6;

    // Initialize allTickets with all possible lucky numbers.
    static {
        // upper limit of 6-digit numbers(exclusive).
        final int const1 = 1000000;
        // Constants needed to get digits at each position.
        final int const2 = 100000;
        final int const3 = 10000;
        final int const4 = 1000;
        final int const5 = 100;
        final int const6 = 10;

        for (int i = 0; i < const1; i++) {
            // get digit on position 1-6
            int i1 = (i / const2) % const6;
            int i2 = (i / const3) % const6;
            int i3 = (i / const4) % const6;
            int i4 = (i / const5) % const6;
            int i5 = (i / const6) % const6;
            int i6 = i % const6;

            // Check if this number is lucky.
            boolean canBeMoskow = (i1 + i2 + i3) == (i4 + i5 + i6);
            boolean canBePiter = (i1 + i3 + i5) == (i2 + i4 + i6);

            // If number is lucky add Ticket based on it to list.
            if (canBeMoskow || canBePiter) {
                String number = "" + i1 + i2 + i3 + i4 + i5 + i6;
                allTickets.add(new Ticket(number));
            }
        }
    }

    /**
     * Allocate an {@code Ticket} object and initializes it
     * with {@code String} .
     * @param number    Ticket number as {@code String}.
     */
    public Ticket(final String number) {
        this.setNumber(number);
    }

    /**
     * Allocate an {@code Ticket} object and initializes it
     * with {@code String} based on given number.
     * Currently redundant for this app.
     * @param number    Ticket number as {@code int}.
     */
    public Ticket(final int number) {
        this(convNumbersToString(number));
    }

    /**
     * Sets this {@code Ticket} number to given value.
     * Ensures it is 6-digit ticket.
     * @param number    desired ticket number.
     */
    public final void setNumber(final String number) {
        if (number.length() != NUMBER_OF_DIGITS) {
            throw new AssertionError("Wrong ticket-number format: " + number);
        }

        for (int i = 0; i < NUMBER_OF_DIGITS; i++) {
            char charI = number.charAt(i);
            if (charI < '0' || charI > '9') {
                throw new AssertionError("Wrong ticket-number format: "
                        + number);
            }
        }

        this.number = number;
    }

    /**
     * Gets ticket number.
     * @return  ticket number as {@code String}.
     */
    public final String getNumber() {
        return number;
    }

    /**
     * Gets set of all possible ticket numbers.
     * @return  all possible ticket numbers as {@code HashSet};
     */
    public static HashSet<Ticket> getAllTickets() {
        return allTickets;
    }

    /**
     * Converts ticket number in {@code int} form to {@code String} form.
     * Currently redundant for this app.
     * @param number    ticket number as {@code int}.
     * @return          ticket number as {@code String}.
     */
    private static String convNumbersToString(final int number) {
        String result = String.valueOf(number);
        int curLength = result.length();

        for (int i = 0; i < (Ticket.NUMBER_OF_DIGITS - curLength); i++) {
            result = "0" + result;
        }

        return result;
    }
}
