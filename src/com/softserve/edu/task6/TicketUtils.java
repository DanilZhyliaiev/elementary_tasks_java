package com.softserve.edu.task6;

import java.util.HashSet;

/**
 * This class provide methods associated with finding ticket
 * 'luckyness' using 'Moscow'/'Piter' techniques.
 * @author Danil Zhyliaiev
 *
 */
public final class TicketUtils {
    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private TicketUtils() { }

    /**
     * Counts number of lucky tickets for specified mode.
     * @param mode  mode that defines way of calculations.
     *              'Moskow' - lucky = sum of 1st three + sum of 2nd three.
     *              'Piter' - lucky = sum of odds + sum of evens.
     * @return      amount of all lucky tickets as {@code int}.
     *              {@code -1} if given mode is unsupported.
     */
    public static int countUpAllLucky(final String mode) {
        HashSet<Ticket> allTickets = Ticket.getAllTickets();
        int luckycount = 0;

        if ("Moskow".equals(mode)) {
            for (Ticket ticket : allTickets) {
                if (isLuckyMoskow(ticket)) {
                    luckycount++;
                }
            }

            return luckycount;
        } else if ("Piter".equals(mode)) {
            for (Ticket ticket : allTickets) {
                if (isLuckyPiter(ticket)) {
                    luckycount++;
                }
            }

            return luckycount;
        } else {
            return -1;
        }
    }

    /**
     * Qualify if given ticket is lucky according to 'Moskow' technique.
     * @param ticket    ticket to verify.
     * @return          {@code true} if ticket is lucky.
     *                  {@code false} if ticket isn't lucky.
     */
    public static boolean isLuckyMoskow(final Ticket ticket) {
        int[] numbers = parseTicketNumber(ticket.getNumber());
        int leftSum = 0;
        int rightSum = 0;

        for (int i = 0; i < Ticket.NUMBER_OF_DIGITS; i++) {
            if (i < (Ticket.NUMBER_OF_DIGITS / 2)) {
                leftSum += numbers[i];
            } else {
                rightSum += numbers[i];
            }
        }

        return leftSum == rightSum;
    }

    /**
     * Qualify if given ticket is lucky according to 'Piter' technique.
     * @param ticket    ticket to verify.
     * @return          {@code true} if ticket is lucky.
     *                  {@code false} if ticket isn't lucky.
     */
    public static boolean isLuckyPiter(final Ticket ticket) {
        int[] numbers = parseTicketNumber(ticket.getNumber());
        int evenSum = 0;
        int oddSum = 0;

        for (int i = 0; i < Ticket.NUMBER_OF_DIGITS; i++) {
            if (i % 2 == 0) {
                evenSum += numbers[i];
            } else {
                oddSum += numbers[i];
            }
        }

        return evenSum == oddSum;
    }

    /**
     * Parses given ticket number to int array.
     * @param number    {@code String} to parse.
     * @return          parsed number as {@code int[]}.
     */
    private static int[] parseTicketNumber(final String number) {
        String[] numberInWords = number.split("");
        int[] numbers = new int[Ticket.NUMBER_OF_DIGITS];

        for (int i = 0; i < Ticket.NUMBER_OF_DIGITS; i++) {
            numbers[i] = Integer.parseInt(numberInWords[i]);
        }

        return numbers;
    }
}
