package com.softserve.edu.task4.input;

import com.softserve.edu.task4.Messages;
import com.softserve.edu.task4.input.file.FileEntryCounter;
import com.softserve.edu.task4.input.file.FileHandler;
import com.softserve.edu.task4.input.file.FileStringReplacer;

import java.io.File;
import java.io.IOException;

/**
 * Class to work with CL arguments.
 * @author - Danil Zhyliaiev
 */
public class InputReader {
    /** Valid number of arguments in count mode. */
    private static final int COUNT_ARGS_NUMBER = 2;
    /** Valid number of arguments in replace mode. */
    private static final int REPLACE_ARGS_NUMBER = 3;
    /** Index of path argument. */
    private static final int PATH_INDEX = 0;
    /** Index of string to search/replace argument. */
    private static final int SEARCH_STRING_INDEX = 1;
    /** Index of string to replace with argument. */
    private static final int REPLACE_STRING_INDEX = 2;

    /**
     * Verify given arguments to meet app requirements.
     * @param args  a {@code String[]} containing Command Line arguments
     *              to be verified.
     */
    private void verifyArgs(final String[] args) {
        if (args.length == 0) {
            System.out.println(Messages.HELP_MESSAGE);

            System.exit(0);
        } else if ((args.length != COUNT_ARGS_NUMBER)
                && (args.length != REPLACE_ARGS_NUMBER)) {
            System.out.println(Messages.WRONG_ARGS_NUMBER_MESSAGE);
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }
    }

    /**
     * Switch between modes ,chooses needed handler and return it.
     * @param args  Command Line arguments.
     * @return      {@code FileHandler}
     */
    public final FileHandler getHandler(final String[] args) {
        File file = new File(args[PATH_INDEX]);

        switch (args.length) {
            case COUNT_ARGS_NUMBER:
                try {
                    return new FileEntryCounter(file, args[SEARCH_STRING_INDEX]);
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    System.out.println(Messages.HOW_TO_GET_HELP);

                    System.exit(1);
                }
                break;
            case REPLACE_ARGS_NUMBER:
                try {
                    return new FileStringReplacer(file,
                            args[SEARCH_STRING_INDEX], args[REPLACE_STRING_INDEX]);
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    System.out.println(Messages.HOW_TO_GET_HELP);

                    System.exit(1);
                }
                break;
            default:
                verifyArgs(args);
                break;
        }

        System.exit(-1);
        return null;
    }
}
