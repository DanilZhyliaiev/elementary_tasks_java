package com.softserve.edu.task4.input.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

/**
 * Class to replace all entries of string with given value.
 * @author - Danil Zhyliaiev
 */
public class FileStringReplacer implements FileHandler {
    /** {@code File} to handle. */
    private File file;
    /** {@code String} to replace. */
    private String strToReplace;
    /** Raplacer {@code String}. */
    private String replaceStr;
    /** {@code List<String>} with file content. */
    private List<String> content;

    /**
     * Allocate a {@code FileStringReplacer} object and initialize it with given parameters.
     * @param file          {@code File} to handle.
     * @param strToReplace  {@code String} to replace.
     * @param replacer      Replacer {@code String}.
     * @throws IOException  .
     */
    public FileStringReplacer(final File file, final String strToReplace,
                              final String replacer) throws IOException {
        setFile(file);
        setStrToReplace(strToReplace);
        setReplaceStr(replacer);
    }

    /**
     * Gets current file handler work with.
     * @return  current {@code File} object.
     */
    public final File getFile() {
        return file;
    }

    /**
     * Sets object file variable to given {@code File}.
     * Reads file content.
     * @param file          desired {@code File}.
     * @throws IOException  .
     */
    public final void setFile(final File file) throws IOException {
        if (file.isFile()) {
            this.file = file;
            readContent();
        } else {
            throw new FileNotFoundException("File '" + file.getAbsolutePath()
                    + "' not found!");
        }
    }

    /**
     * Gets current string to search.
     * @return  object {@code strToReplace} variable.
     */
    public final String getStrToReplace() {
        return strToReplace;
    }

    /**
     * Sets string to replace.
     * @param strToReplace desired strToReplace value.
     */
    public final void setStrToReplace(final String strToReplace) {
        this.strToReplace = strToReplace;
    }

    /**
     * Gets current replacer string.
     * @return  object {@code getReplaceStr} variable.
     */
    public final String getReplaceStr() {
        return replaceStr;
    }

    /**
     * Sets replacer string.
     * @param replaceStr    desired replaceStr value.
     */
    public final void setReplaceStr(final String replaceStr) {
        this.replaceStr = replaceStr;
    }

    /**
     * Gets current file content.
     * @return  file content as {@code List<String>}.
     */
    public final List<String> getContent() {
        return content;
    }

    /**
     * Read all lines of current file variable to content variable.
     * @throws IOException  .
     */
    private void readContent() throws IOException {
        this.content = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
    }

    /**
     * Replaces all occurrences of {@code String strToReplace}
     * with {@code String replaceStr} and write it to {@code file}.
     * @throws IOException .
     */
    public final void replace() throws IOException {
        if (content.isEmpty() || strToReplace.isEmpty()) {
            throw new AssertionError("File and/or query string is empty");
        }
        if (!file.canWrite()) {
            throw new AssertionError("File " + file.getAbsolutePath() + " is read-only");
        }

        List<String> replaced = new LinkedList<>();
        for (String str: content) {
            replaced.add(str.replace(strToReplace, replaceStr));
        }

        Files.write(file.toPath(), replaced, StandardCharsets.UTF_8);
    }
}
