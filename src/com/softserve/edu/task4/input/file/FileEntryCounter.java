package com.softserve.edu.task4.input.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

/**
 * Class to count entries of string in file.
 * @author - Danil Zhyliaiev
 */
public class FileEntryCounter implements FileHandler {
    /** {@code File} to handle. */
    private File file;
    /** {@code String} to search. */
    private String searchStr;
    /** {@code List<String>} with file content. */
    private List<String> content;

    /**
     * Allocate a {@code FileEntryCounter} object and initialize it with given parameters.
     * @param file          {@code File} to handle.
     * @param searchStr     {@code String} to search.
     * @throws IOException  .
     */
    public  FileEntryCounter(final File file, final String searchStr) throws  IOException {
        setFile(file);
        setSearchStr(searchStr);
    }

    /**
     * Gets current file handler work with.
     * @return  current {@code File} object.
     */
    public final File getFile() {
        return file;
    }

    /**
     * Sets object file variable to given {@code File}.
     * Reads file content.
     * @param file          desired {@code File}.
     * @throws IOException  .
     */
    public final void setFile(final File file) throws IOException {
        if (file.isFile()) {
            this.file = file;
            readContent();
        } else {
            throw new FileNotFoundException("File '" + file.getAbsolutePath()
                    + "' not found!");
        }
    }

    /**
     * Gets current string to search.
     * @return  object {@code searchStr} variable.
     */
    public final String getSearchStr() {
        return searchStr;
    }

    /**
     * Sets string to search.
     * @param searchStr desired searchStr value.
     */
    public final void setSearchStr(final String searchStr) {
        this.searchStr = searchStr;
    }

    /**
     * Gets current file content.
     * @return  file content as {@code List<String>}.
     */
    public final List<String> getContent() {
        return content;
    }

    /**
     * Read all lines of current file variable to content variable.
     * @throws IOException  .
     */
    private void readContent() throws IOException {
        this.content = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
    }

    /**
     * Count number of occurrences based on object searchStr and file variables.
     * @return      number of occurrences as {@code int}.
     */
    public final int countEntries() {
        int entryCount = 0;

        if (content.isEmpty() || searchStr.isEmpty()) {
            return -1;
        }

        for (String str : content) {
            int index = 0;

            // Go thru the line until there is matches with searchStr
            while ((index = str.indexOf(searchStr, index)) != -1) {
                entryCount++;
                index += searchStr.length();
            }
        }

        return entryCount;
    }
}
