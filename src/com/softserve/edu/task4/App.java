package com.softserve.edu.task4;

import com.softserve.edu.task4.input.InputReader;
import com.softserve.edu.task4.input.file.FileHandler;
import com.softserve.edu.task4.output.ResultWriter;

/**
 * This app parses file to count/replace given value.
 * @author Danil Zhyliaiev
 *
 */
public final class App {
    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private App() { }

    /**
     * Task4 entry point.
     * @param args
     *      Parameters From Command Line.
     */
    public static void main(final String[] args) {

        InputReader inputReader = new InputReader();

        FileHandler fileHandler = inputReader.getHandler(args);

        new ResultWriter().print(fileHandler);
    }
}

