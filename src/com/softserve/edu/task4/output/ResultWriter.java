package com.softserve.edu.task4.output;

import com.softserve.edu.task4.input.file.FileEntryCounter;
import com.softserve.edu.task4.input.file.FileHandler;
import com.softserve.edu.task4.input.file.FileStringReplacer;

import java.io.IOException;

/**
 * Class to print provide output based on chosen in arguments mode.
 * @author - Danil Zhyliaiev
 */
public class ResultWriter {
    /**
     * Chooses needed implementation of print() method based on given handler.
     * @param fileHandler   {@code FileHandler}.
     */
    public final void print(final FileHandler fileHandler) {
        if (fileHandler.getClass() == FileEntryCounter.class) {
            printCount((FileEntryCounter) fileHandler);
        } else if (fileHandler.getClass() == FileStringReplacer.class) {
            printReplaced((FileStringReplacer) fileHandler);
        }
    }

    /**
     * Print COUNT mode report.
     * @param fileEntryCounter  COUNT mode file handler.
     */
    public final void printCount(final FileEntryCounter fileEntryCounter)  {
        System.out.println("You've chosen COUNT mode.");
        System.out.println();

        System.out.println("The number of occurrences of the string '"
                + fileEntryCounter.getSearchStr() + "' in the file '"
                + fileEntryCounter.getFile().getAbsolutePath() + "' = "
                + fileEntryCounter.countEntries() + "");
    }

    /**
     * Print REPLACE mode report.
     * @param fileStringReplacer    REPLACE moode file handler.
     */
    public final void printReplaced(final FileStringReplacer fileStringReplacer) {
        System.out.println("You've chosen REPLACE mode.");
        System.out.println();

        try {
            fileStringReplacer.replace();
        } catch (IOException e) {
            System.out.println(e.getMessage());

            System.exit(-1);
        }

        System.out.println("All occurrences of the string '"
                + fileStringReplacer.getStrToReplace() + "' in file '"
                + fileStringReplacer.getFile().getAbsolutePath()
                + "' are replaced with '"
                + fileStringReplacer.getReplaceStr() + "'.");
    }
}
