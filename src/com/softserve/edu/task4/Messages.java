package com.softserve.edu.task4;

/**
 * Class-container for application default messages.
 * @author - Danil Zhyliaiev
 */
public final class Messages {
    /** Message used before shut down program. Used to provide help to user. */
    public static final String HELP_MESSAGE = "How to use:\n"
            + "Program has 2 modes:\n"
            + "    1. COUNT - counts the number of occurrences of the string in file(case sensitive!).\n"
            + "    2. REPLACE - replaces all occurrences of string with another string(case sensitive!).\n"
            + "To get program in a certain mode - run it with arguments in following formats:\n"
            + "COUNT: '<filepath> <string to count>'\n"
            + "REPLACE: '<filepath> <string to be replaced> <the replacement string>'\n"
            + "To view this help run program without arguments.";
    /** Message used before shut down program. Used to inform user how to get help. */
    public static final String HOW_TO_GET_HELP = "Run program with no arguments "
            + "to get a detailed guide.";
    /** Message used before shut down program. Used to inform user about allowed argument formats. */
    public static final String WRONG_ARGS_NUMBER_MESSAGE = "Wrong number of arguments.\n"
            + "Only two argument formats are allowed:\n"
            + "1. <filepath> <string to count>\n"
            + "2. <filepath> <string to be replaced> <the replacement string>";

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private Messages() { }
}
