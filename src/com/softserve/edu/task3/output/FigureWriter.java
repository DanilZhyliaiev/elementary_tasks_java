package com.softserve.edu.task3.output;

import com.softserve.edu.task3.figure.Figure;

import java.util.Collections;
import java.util.List;

/**
 * Class to provides methods to print figures in specified format.
 * @author - Danil Zhyliaiev
 */
public class FigureWriter {
    /**
     * Prints list of figures using special output format.
     * @param figures   list of {@code Figure}'s.
     */
    public final void print(final List<Figure> figures) {

        System.out.println("============= Triangles list: ===============");

        if (figures.size() == 0) {
            System.out.println("NONE");
        } else {
            for (int i = 0; i < figures.size(); i++) {
                System.out.print((i + 1) + "" + figures.get(i).toString());
                System.out.println();
            }
        }
    }

    /**
     * Prints list of figures in ascending order using special output format.
     * @param figures   list of {@code Figure}'s.
     */
    public final void printAscending(final List<Figure> figures) {
        Collections.sort(figures);

        print(figures);
    }

    /**
     * Prints list of figures in descending order using special output format.
     * @param figures   list of {@code Figure}'s.
     */
    public final void printDescending(final List<Figure> figures) {
        Collections.sort(figures, Collections.reverseOrder());

        print(figures);
    }
}
