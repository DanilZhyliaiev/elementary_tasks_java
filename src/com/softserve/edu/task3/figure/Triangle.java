package com.softserve.edu.task3.figure;

import java.text.DecimalFormat;

/**
 * An abstract representation of triangle.
 * @author Danil Zhyliaiev
 *
 */
public class Triangle extends Figure  {
    /** Invalid side-length error message. */
    private static final String INVALID_SIDE_LENGTH = "Triangle side length cannot be negative/zero";
    /** A side violates triangle inequality. */
    private static final String INEQUALITY_VIOLATED_A = "Triangle inequality violated."
            + " A cannot be greater that B+C";
    /** B side violates triangle inequality. */
    private static final String INEQUALITY_VIOLATED_B = "Triangle inequality violated."
            + " B cannot be greater that A+C";
    /** C side violates triangle inequality. */
    private static final String INEQUALITY_VIOLATED_C = "Triangle inequality violated."
            + " C cannot be greater that A+B";
    /** Triangle name. */
    private String name;
    /** Triangle first side length. */
    private double sideA;
    /** Triangle second side length. */
    private double sideB;
    /** Triangle third side length. */
    private double sideC;

    /**
     * Allocate an {@code Triangle} object and initializes it
     * with {@code name, sideA, sideB, sideC} parameters.
     * @param name      {@code String} object name.
     * @param sideA     positive {@code double}.
     * @param sideB     positive {@code double}.
     * @param sideC     positive {@code double}.
     */
    public Triangle(final String name, final double sideA,
                    final double sideB, final double sideC) {
        setName(name);
        setSideA(sideA);
        setSideB(sideB);
        setSideC(sideC);
        ineqCheck();

    }

    /**
     * Gets {@code Triangle} name.
     * @return  {@code String} name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Sets the name parameter to the given value.
     * Ignores case.
     * @param name  desired name value.
     */
    public final void setName(final String name) {
        this.name = name.toLowerCase();
    }

    /**
     * Gets {@code Triangle} sideA length.
     * @return  {@code double} sideA.
     */
    public final double getSideA() {
        return sideA;
    }

    /**
     * Sets the sideA parameter to the given value.
     * Validates that it is positive number.
     * @param sideA  desired sideA length as {@code double}.
     */
    public final void setSideA(final double sideA) {
        if (sideA <= 0) {
            throw new IllegalArgumentException(INVALID_SIDE_LENGTH);
        } else {
            this.sideA = sideA;
        }
    }

    /**
     * Gets {@code Triangle} sideB length.
     * @return  {@code double} sideB.
     */
    public final double getSideB() {
        return sideB;
    }

    /**
     * Sets the sideB parameter to the given value.
     * Validates that it is positive number.
     * @param sideB  desired sideB length as {@code double}.
     */
    public final void setSideB(final double sideB) {
        if (sideB <= 0) {
            throw new IllegalArgumentException(INVALID_SIDE_LENGTH);
        } else {
            this.sideB = sideB;
        }
    }

    /**
     * Gets {@code Triangle} sideC length.
     * @return  {@code double} sideC.
     */
    public final double getSideC() {
        return sideC;
    }

    /**
     * Sets the sideC parameter to the given value.
     * Validates that it is positive number.
     * @param sideC  desired sideC length as {@code double}.
     */
    public final void setSideC(final double sideC) {
        if (sideC <= 0) {
            throw new IllegalArgumentException(INVALID_SIDE_LENGTH);
        } else {
            this.sideC = sideC;
        }
    }

    /**
     * Gets {@code Triangle} area using Heron's formula.
     * @return  area as {@code double}.
     */
    public final double getArea() {
        ineqCheck();                            // in case user has changed sidelength manually.
        double s = (sideA + sideB + sideC) / 2; // semiperimeter of the triangle.
        return Math.sqrt(s * (s - sideA) * (s - sideB) * (s - sideC));
    }


    /**
     * Checks if this triangle inequality isn't violated.
     */
    private void ineqCheck() {
        ineqCheck(sideA, sideB, sideC);
    }

    /**
     * Checks if given values can form a triangle(inequality isn't violated).
     * @param a     {@code double} first value.
     * @param b     {@code double} second value.
     * @param c     {@code double} third value.
     */
    private void ineqCheck(final double a, final double b, final double c) {
        if (a > (b + c)) {
            throw new IllegalArgumentException(INEQUALITY_VIOLATED_A);
        } else if (b > (a + c)) {
            throw new IllegalArgumentException(INEQUALITY_VIOLATED_B);
        } else if (c > (a + b)) {
            throw new IllegalArgumentException(INEQUALITY_VIOLATED_C);
        }
    }

    /**
     * Compares the triangle to given Figure.
     * @param second    Figure to compare with.
     * @return          {@code 0} this triangle is  equal to {@code second};
     *                  a value less than {@code 0} if this triangle is less
     *                  than {@code second};
     *                  and a value greater than {@code 0} if this triangle
     *                  is greater than {@code second}.
     */
    @Override
    public final int compareTo(final Figure second) {
        // compares Triangles by their area value
        return Double.compare(this.getArea(), second.getArea());
    }

    @Override
    public final String toString() {
        return ("[Triangle " + this.name + "]: " + new DecimalFormat("#0.00")
                .format(this.getArea()) + " cm²");
    }
}
