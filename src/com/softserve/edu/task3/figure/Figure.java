package com.softserve.edu.task3.figure;

/**
 * Abstrack representation of a figure.
 * All figures have to implement {@code getArea()} method and
 * interface {@code Comparable}.
 */
public abstract class Figure implements  Comparable<Figure> {
    /**
     * Calculate area of the figure.
     * @return  area as {@code double}.
     */
    public abstract double getArea();
}
