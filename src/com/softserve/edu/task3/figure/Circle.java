package com.softserve.edu.task3.figure;

import java.text.DecimalFormat;

/**
 * Test class to verify application scalability. Javadoc omitted.
 */
public class Circle extends Figure {
    private String name;
    private double radius;

    public Circle(String name, double radius) {
        setName(name);
        setRadius(radius);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.toLowerCase();
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        if (radius <= 0) {
            throw new IllegalArgumentException("Circle radius length cannot be negative/zero");
        } else {
        this.radius = radius;
        }
    }

    @Override
    public double getArea() {
        return Math.PI * (radius * radius);
    }

    @Override
    public int compareTo(Figure second) {
        return Double.compare(this.getArea(), second.getArea());
    }

    @Override
    public String toString() {
        return ("[Circle " + this.name + "]: " + new DecimalFormat("#0.00")
                .format(this.getArea()) + " cm²");
    }
}
