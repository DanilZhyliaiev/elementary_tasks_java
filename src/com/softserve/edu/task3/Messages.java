package com.softserve.edu.task3;

/**
 * Class-container for application default messages.
 * @author - Danil Zhyliaiev
 */
public final class Messages {
    /** Message used before shut down program. Used to provide help to user. */
    public static final String HELP_MESSAGE = "How to use:\n"
            + "Fill in triangle name and sizes using next format:\n"
            + "<name>, <sidelength>, <sidelength>, <sidelength>\n"
            + "- Only positive numbers(including both floating-point numbers "
            + "and integers) are allowed as triangle sidelength;\n"
            + "- Only dot('.') can be used as a decimal mark;\n"
            + "- All input is case/spaces/tabs insensitive;\n"
            + "- Triangle area is calcualted using Heron's formula;\n"
            + "- To continue adding triangles answer 'y' or 'yes'"
            + "(case/spaces/tabs insensitive);\n"
            + "After all triangles are added - program will print "
            + "them out in descending order(based on their area)\n"
            + "To view this help run program without arguments.\n";
    /** Message to display if input values cannot be parsed to a number. */
    public static final String WRONG_NUMBER_FORMAT = "Wrong number format."
            + " Only positive numbers are accepted";


    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private Messages() { }
}
