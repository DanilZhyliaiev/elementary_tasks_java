package com.softserve.edu.task3.input;

import com.softserve.edu.task3.figure.Figure;

import java.io.IOException;

/**
 *  Interface for reading figures.
 *  The only method subclass need to implement is parse(String input).
 * @param <T>   Figure subclass.
 */
public interface FigureReader<T extends Figure> {
    /**
     * Parse user input to specified figure.
     * @param input         to parse in figure.
     * @return              user input as figure.
     * @throws IOException  .
     */
     T parse(String input) throws IOException;

}
