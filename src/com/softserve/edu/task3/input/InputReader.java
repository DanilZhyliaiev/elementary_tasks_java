package com.softserve.edu.task3.input;

import com.softserve.edu.task3.Messages;
import com.softserve.edu.task3.figure.Figure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Class to work with all input. Including arguments and user input.
 * @author - Danil Zhyliaiev
 */
public class InputReader implements AutoCloseable {
    /** Message used to inform user how to get help. */
    private static final String HOW_TO_GET_HELP = "Run program with no arguments to get"
            + " a detailed guide.\n";
    /** BufferedReader to read user input. */
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    /** Specified FigureReader to parse desired figures. */
    private FigureReader figureReader;

    /**
     * Allocate {@code InputReader} object and initializes reader.
     * @param figureReader  Specified {@code FigureReader} to use.
     */
    public InputReader(final FigureReader figureReader) {
        this.figureReader = figureReader;
    }

    /**
     * Print help message if no arguments were given.
     * @param args  CL arguments.
     */
    public final void verifyHelpNeeded(final String[] args) {
        if (args.length == 0) {
            System.out.println(Messages.HELP_MESSAGE);
        }
    }

    /**
     * Request user to input a figure. If user wants to proceed - start over.
     * @return              Result entered figures as {@code ArrayList}.
     * @throws IOException  .
     */
    public final ArrayList<Figure> readInput() throws IOException {
        ArrayList<Figure> figures = new ArrayList<>();
        boolean proceed = true;

        while (proceed) {
            System.out.println("Enter figure name and sizes:");
            String input = reader.readLine();

            try {
                Figure figure1 = figureReader.parse(input);
                figures.add(figure1);
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
                // get current figure name.
                String figureType = e.getStackTrace()[0]
                        .getFileName()
                        .replace("Reader", "")
                        .replace(".java", "");
                System.out.println(figureType + " will not be added!");
                System.out.println(HOW_TO_GET_HELP);
            }


            // Reads line, trims all whitespace and converts to lower case.
            proceed = wantToProceed();
        }

        reader.close();

        return figures;
    }

    /**
     * Ask user if he want to proceed. Accepts 'y' and 'yes'
     * as positive answers.
     * Case/whitespace insensitive.
     * @return              {@code boolean} answer if user wants to proceed.
     * @throws IOException  .
     */
    private boolean wantToProceed() throws IOException {
        System.out.println("Do you want to proceed?");
        // Reads line, trims all whitespace and converts to lower case.
        String proceed = reader.readLine().toLowerCase().replaceAll("\\s", "");

        return ("y".equals(proceed) || "yes".equals(proceed));
    }

    @Override
    public final void close() throws IOException {
        reader.close();
    }
}
