package com.softserve.edu.task3.input;

import com.softserve.edu.task3.figure.Circle;

/**
 * Test class to verify scalability. Javadoc omitted.
 */
public class CircleReader implements FigureReader {

    public final Circle parse(final String input) {
        String params[] = parseCircle(input);

        // constants
        final int indexOfName = 0;
        final int indexOfRadius = 1;

        String name = params[indexOfName];

        double radius;
        try {
            radius = Double.parseDouble(params[indexOfRadius]);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Wrong number format."
                    + " Only positive numbers are accepted");
        }

        return new Circle(name, radius);
    }

    private String[] parseCircle(String input) {
        final int validParamLength = 2;
        String[] circleParams = input.replaceAll("\\s", "").split(",");

        if (circleParams.length != validParamLength) {
            throw new IllegalArgumentException("Incorrect line fromat."
                    + " Please, use next format:\n"
                    + "<name>, <radius-length>");
        }
        return circleParams;
    }
}
