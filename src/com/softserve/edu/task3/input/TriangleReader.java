package com.softserve.edu.task3.input;


import com.softserve.edu.task3.Messages;
import com.softserve.edu.task3.figure.Triangle;

/**
 * Class to parse {@code Triangle} form user input.
 */
public class TriangleReader implements FigureReader {
    /** Message displayed if input was given in unsupported format. */
    private static final String INCORRECT_INPUT_FORMAT = "Incorrect line fromat."
            + " Please, use next format:\n"
            + "<name>, <side-length>, <side-length>, <side-length>";
    /**
     * Parses Triangle from user input and return it.
     * @param input         {@code String} to parse Triangle attributes.
     * @return              user input as {@code Triangle}.
     */
    public final Triangle parse(final String input) {
        String[] params = parseFormat(input);

        // local constants according to '<name>, <side-length>, <side-length>, <side-length>' format.
        final int indexOfName = 0;
        final int indexOfA = 1;
        final int indexOfB = 2;
        final int indexOfC = 3;

        String name = params[indexOfName];

        double a;
        double b;
        double c;
        try {
            a = Double.parseDouble(params[indexOfA]);
            b = Double.parseDouble(params[indexOfB]);
            c = Double.parseDouble(params[indexOfC]);
        } catch (NumberFormatException e) {
            throw new NumberFormatException(Messages.WRONG_NUMBER_FORMAT);
        }

        return new Triangle(name, a, b, c);
    }

    /**
     * Parse String to get all Triangle attributes.
     * Trim all whitespace in line, split it to array using ',' as separator.
     * @param input  {@code String} in format
     *              <name>, <side-length>, <side-length>, <side-length>".
     * @return      Triangle attributes as {@code String[]}.
     */
    private static String[] parseFormat(final String input) {
        // constant number used to verify if number of arguments is valid.
        final int validParamLength = 4;
        String[] triangleParams = input.replaceAll("\\s", "").split(",");

        if (triangleParams.length != validParamLength) {
            throw new IllegalArgumentException(INCORRECT_INPUT_FORMAT);
        }
        return triangleParams;
    }
}
