package com.softserve.edu.task3;

import com.softserve.edu.task3.figure.Figure;
import com.softserve.edu.task3.input.InputReader;
import com.softserve.edu.task3.input.TriangleReader;
import com.softserve.edu.task3.output.FigureWriter;

import java.io.IOException;
import java.util.ArrayList;

/**
 * This app convert user input to triangles(figures). In the end of input print
 * all figures in descending order(based on area).
 * @author Danil Zhyliaiev
 *
 */
public final class App {
    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private App() { }

    /**
     * Task3 entry point.
     * @param args
     *      Parameters From Command Line.
     * @throws IOException .
     */
    public static void main(final String[] args) throws IOException {
        try (InputReader inputReader = new InputReader(new TriangleReader())) {
        inputReader.verifyHelpNeeded(args);

        ArrayList<Figure> figures = inputReader.readInput();

        new FigureWriter().printDescending(figures);
        }
    }
}
