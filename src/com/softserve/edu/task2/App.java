package com.softserve.edu.task2;

import com.softserve.edu.task2.input.EnvelopeReader;
import com.softserve.edu.task2.input.InputReader;
import com.softserve.edu.task2.input.RectangularEnvelopeReader;

import java.io.IOException;

/**
 * This app calculate if given envelopes can fit in each other.
 * @author Danil Zhyliaiev
 *
 */
public final class App {
    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private App() { }

    /**
     * Task2 entry point.
     * @param args
     *      Parameters From Command Line.
     * @throws IOException .
     */
    public static void main(final String[] args) throws IOException {
        try (RoundEnvelopeReader envelopeReader = new RectangularEnvelopeReader();
             InputReader inputReader = new InputReader()) {

        // check if help needed (args.length = 0)
        inputReader.verifyHelpNeeded(args);

        // read figures and check if they fit in each other.
        inputReader.readInput(envelopeReader);
        }
    }
}
