package com.softserve.edu.task2.envelope;

/**
 * Shows that class objects can fit in each other.
 * @param <T>   object type.
 */
public interface Fittable<T>  {
    /**
     * Defines if object can fit in another.
     * @param o     object to fit in.
     * @return      {@code true} if object can fit in another.
     *              {@code false} otherwise.
     */
    boolean fitTo(T o);
}
