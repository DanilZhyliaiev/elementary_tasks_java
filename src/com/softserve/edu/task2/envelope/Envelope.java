package com.softserve.edu.task2.envelope;

/**
 * Abstract representation of an envelope.
 */
public abstract class Envelope implements Fittable {
    /**
     * Calculate envelope area.
     * @return  area as {@code double}.
     */
    public abstract double getArea();
}
