package com.softserve.edu.task2.envelope;

/**
 * Test class to verify scalability.
 * @author Danil Zhyliaiev
 *
 */
public class RoundEnvelope extends Envelope {
    /**  Envelope radius. */
    private double radius;

    /**
     * Allocate an {@code RoundEnvelope} object and initializes it
     * with {@code radius} parameter.
     * @param radius    positive {@code double}.
     */
    public RoundEnvelope(final double radius) {
        setRadius(radius);
    }

    /**
     * Sets the radius parameter to the given {@code height} value.
     * @param radius     desired radius value.
     */
    public final void setRadius(final double radius) {
        if (!isValidValue(radius)) {
            throw new IllegalArgumentException("Height value cannot be negative/zero");
        }
        this.radius = radius;
    }

    /**
     * Gets {@code RoundEnvelope} area.
     * @return  area as {@code double}.
     */
    @Override
    public final double getArea() {
        return (Math.PI * (radius * radius));
}

    /**
     * Checks if given value is valid envelope radius.
     * @param radius        {@code int} to validate.
     * @return              {@code true} if value is valid.
     *                      {@code false} if value is invalid.
     */
    private boolean isValidValue(final double radius) {
        return radius > 0;
    }

    /**
     * Checks if this {@code RoundEnvelope} can be put in another.
     * @param o     {@code Object} to fit in.
     * @return      {@code true} if this {@code RoundEnvelope} fit in {@code o}.
     *              {@code false} if this {@code RoundEnvelope}
     *              doesn't fit in {@code o}.
     */
    @Override
    public final boolean fitTo(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        return getArea() <= ((RoundEnvelope) o).getArea();
    }
}
