package com.softserve.edu.task2.envelope;

/**
 * An abstract representation of rectangular envelope.
 * @author Danil Zhyliaiev
 *
 */
public class RectangularEnvelope extends Envelope {
    /** Envelope height. */
    private double height;
    /** Envelope width. */
    private double width;

    /**
     * Allocate an {@code RectangularEnvelope} object and initializes it
     * with {@code height} and {@code width} parameters.
     * @param   height    positive {@code double}.
     * @param   width     positive {@code double}.
     */
   public RectangularEnvelope(final double height, final double width) {
        setHeight(height);
        setWidth(width);
    }

   /**
    * Sets the height parameter to the given {@code height} value.
    * @param height     desired height value.
    */
   public final void setHeight(final double height) {
       if (isValidValue(height)) {
           this.height = height;
       } else {
           throw new IllegalArgumentException("Height value cannot be negative/zero");
       }
   }

   /**
    * Gets {@code RectangularEnvelope} height.
    * @return   height as {@code double}.
    */
   public final double getHeight() {
       return height;
   }

   /**
    * Sets the width parameter to the given {@code width} value.
    * @param width      desired width value.
    */
    public final void setWidth(final double width) {
        if (isValidValue(width)) {
            this.width = width;
        } else {
            throw new IllegalArgumentException("Width value cannot be negative/zero");
        }
    }

    /**
     * Gets {@code RectangularEnvelope} width.
     * @return  width as {@code double}.
     */
    public final double getWidth() {
        return width;
    }


    /**
     * Gets {@code RectangularEnvelope} diagonal.
     * @return  diagonal as {@code double}.
     */
    public final double getDiagonal() {
        return Math.sqrt((height * height) + (width * width));
    }

    /**
     * Gets {@code RectangularEnvelope} area.
     * @return  area as {@code double}.
     */
    @Override
    public final double getArea() {
        return width * height;
    }

    /**
     * Checks if given value is valid envelope sidelength.
     * @param sidelength    {@code int} to validate.
     * @return              {@code true} if value is valid.
     *                      {@code false} if value is invalid.
     */
    public static boolean isValidValue(final double sidelength) {
        return sidelength > 0;
    }

    /**
     * Checks if this {@code RectangularEnvelope} can be put in another.
     * @param o     {@code Object} to fit in.
     * @return      {@code true} if this {@code RectangularEnvelope} fit in {@code o}.
     *              {@code false} if this {@code RectangularEnvelope}
     *              doesn't fit in {@code o}.
     */
    @Override
    public final boolean fitTo(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RectangularEnvelope env = (RectangularEnvelope) o;

        double a = Math.min(this.height, this.width);   // this. smaller size
        double b = Math.max(this.height, this.width);   // this. larger size
        double c = Math.min(env.height, env.width);     // env smaller size
        double d = Math.max(env.height, env.width);     // env larger size

        if (this.getArea() <= env.getArea()) {
            if (b <= d) {
                return a <= c;
            } else {
                double leftPart = Math.pow(((d + c) / (b + a)), 2)
                        + Math.pow(((d - c) / (b - a)), 2);
                return leftPart >= 2;
            }
        } else {
            return false;
        }
    }
}
