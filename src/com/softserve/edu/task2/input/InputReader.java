package com.softserve.edu.task2.input;

import com.softserve.edu.task2.Messages;
import com.softserve.edu.task2.envelope.Fittable;
import com.softserve.edu.task2.output.EnvelopePrinter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class to work with all input. Including arguments and user input.
 * @author - Danil Zhyliaiev
 */
public class InputReader implements AutoCloseable {
    /** BufferedReader to read user input. */
    private BufferedReader reader;

    /**
     * Allocate {@code InputReader} object, check given arguments
     * and initializes reader.
     */
    public InputReader() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * Print help message if no arguments were given.
     * @param args  CL arguments.
     */
    public final void verifyHelpNeeded(final String[] args) {
        if (args.length == 0) {
            System.out.println(Messages.HELP_MESSAGE);
        }
    }

    /**
     * Request user to input two envelopes. Send them to {@code EnvelopeWriter}.
     * If user wants to proceed - start over.
     * @param envelopeReader  Specified envelope reader.
     * @throws IOException  .
     */
    public final void readInput(final EnvelopeReader envelopeReader)
            throws IOException {
        boolean proceed = true;
        EnvelopePrinter printer = new EnvelopePrinter();

        while (proceed) {
            System.out.println(Messages.REQUEST_FIRST_SIDE);
            Fittable figure1 =  envelopeReader.parse();

            System.out.println(Messages.REQUEST_SECOND_SIDE);
            Fittable figure2 = envelopeReader.parse();


            printer.printIfCanFit(figure1, figure2);

            // Reads line, trims all whitespace and converts to lower case.
            proceed = wantToProceed();
        }

        reader.close();
    }

    /**
     * Ask user if he want to proceed. Accepts 'y' and 'yes'
     * as positive answers.
     * Case/whitespace insensitive.
     * @return              {@code boolean} answer if user wants to proceed.
     * @throws IOException  .
     */
    private boolean wantToProceed() throws IOException {
        System.out.println(Messages.REQUEST_TO_PROCEED);
        // Reads line, trims all whitespace and converts to lower case.
        String proceed = reader.readLine().toLowerCase().replaceAll("\\s", "");

        return ("y".equals(proceed) || "yes".equals(proceed));
    }

    @Override
    public final void close() throws IOException {
        reader.close();
    }
}
