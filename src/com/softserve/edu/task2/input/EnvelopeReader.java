package com.softserve.edu.task2.input;

import com.softserve.edu.task2.envelope.Fittable;

import java.io.IOException;

/**
 *  Abstract class for reading envelopes.
 *  The only method subclass need to implement is parse().
 * @param <T>
 */
public interface EnvelopeReader<T extends Fittable> extends AutoCloseable {
    /**
     * Parse user input to needed object.
     * @return              user input as object.
     * @throws IOException  .
     */
     T parse() throws IOException;

}
