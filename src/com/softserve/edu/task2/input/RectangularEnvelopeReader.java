package com.softserve.edu.task2.input;

import com.softserve.edu.task2.Messages;
import com.softserve.edu.task2.envelope.RectangularEnvelope;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class to parse Rectangular Envelope form user input.
 */
public class RectangularEnvelopeReader implements EnvelopeReader {
    /** Buffered reader to read user input. */
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Parses RectangularEnvelope from user input and return it.
     * @return              user input as {@code RectangularEnvelope}.
     * @throws IOException  .
     */
    public final RectangularEnvelope parse() throws IOException {
        double height;
        double width;

        System.out.println("Height:");
        height = requestEnvelopeSide();

        System.out.println("Width:");
        width = requestEnvelopeSide();


        return new RectangularEnvelope(height, width);
    }

    /**
     * This method request user input and verifies that it can be used as
     * envelope side-length.
     * @return Value of user input as {@code double}.
     * @throws IOException .
     */
    private double requestEnvelopeSide() throws IOException {
        double result = readDouble();

        // If  number is negative - request a new value.
        if (!RectangularEnvelope.isValidValue(result)) {
            System.out.println(Messages.NEGATIVE_INPUT_ERROR);
            result = requestEnvelopeSide();
        }

        return result;
    }

    /**
     * Request user input and tries to parse double form it.
     * Work with both comma and dot separators.
     * If user input cannot be parsed - print message and start over.
     * @return      user input as {@code double}.
     * @throws IOException .
     */
    private double readDouble() throws IOException {
        double result;
        String input = reader.readLine();

        try {
            result = Double.parseDouble(input);
        } catch (NumberFormatException e) {
            try {
                input = input.replaceAll(",", ".");
                result = Double.parseDouble(input);
            } catch (NumberFormatException e2) {
                System.out.println(Messages.WRONG_NUMBER_FORMAT_ERROR);

                result = readDouble();
            }
        }
        return result;
    }

    @Override
    public final void close() throws IOException {
        reader.close();
    }
}
