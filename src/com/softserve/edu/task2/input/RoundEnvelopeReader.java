package com.softserve.edu.task2.input;

import com.softserve.edu.task2.Messages;
import com.softserve.edu.task2.envelope.RectangularEnvelope;
import com.softserve.edu.task2.envelope.RoundEnvelope;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class to parse Round Envelope from user input.
 */
public class RoundEnvelopeReader implements EnvelopeReader {
    /** Buffered reader to read user input. */
    private BufferedReader reader;

    /**
     * Allocate {@code  RoundEnvelopeReader} object and initializes reader variable.
     */
    public RoundEnvelopeReader() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * Parses RoundEnvelope from user input and return it.
     * @return              user input as {@code RoundEnvelope}.
     * @throws IOException  .
     */
    public final RoundEnvelope parse() throws IOException {
        double radius;

        System.out.println("Radius:");
        radius = requestCircleRadius();

        return new RoundEnvelope(radius);
    }

    /**
     * This method request user input and verifies that it can be used as
     * round envelope radius.
     * @return Value of user input as {@code double}.
     * @throws IOException .
     */
    private double requestCircleRadius() throws IOException {
        double result = readDouble();

        // If  number is negative - request a new value.
        if (!RectangularEnvelope.isValidValue(result)) {
            System.out.println(Messages.NEGATIVE_INPUT_ERROR);
            result = requestCircleRadius();
        }

        return result;
    }

    /**
     * Request user input and tries to parse double form it.
     * Work with both comma and dot separators.
     * If user input cannot be parsed - print message and start over.
     * @return      user input as {@code double}.
     * @throws IOException .
     */
    private double readDouble() throws IOException {
        double result;
        String input = reader.readLine();

        try {
            result = Double.parseDouble(input);
        } catch (NumberFormatException e) {
            try {
                input = input.replaceAll(",", ".");
                result = Double.parseDouble(input);
            } catch (NumberFormatException e2) {
                System.out.println(Messages.WRONG_NUMBER_FORMAT_ERROR);

                result = readDouble();
            }
        }
        return result;
    }

    @Override
    public final void close() throws IOException {
        reader.close();
    }
}
