package com.softserve.edu.task2.output;

import com.softserve.edu.task2.envelope.Fittable;

/**
 * Class to provide output in desired format.
 * @author - Danil Zhyliaiev
 */
public class EnvelopePrinter {
    /**
     *  Prints if two envelopes can fit in each other.
     * @param envelope1   first {@code Envelope}.
     * @param envelope2   second {@code Envelope}.
     */
    public final void printIfCanFit(final Fittable envelope1,
                                    final Fittable envelope2) {
        System.out.println(String.format("FIRST %s can be put in SECOND? - %S",
                envelope1.getClass().getSimpleName(),
                envelope1.fitTo(envelope2)));
        System.out.println(String.format("SECOND %s can be put in FIRST? - %S",
                envelope2.getClass().getSimpleName(),
                envelope2.fitTo(envelope1)));
    }
}
