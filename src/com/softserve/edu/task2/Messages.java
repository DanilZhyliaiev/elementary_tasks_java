package com.softserve.edu.task2;

/**
 * Class-container for application default messages.
 * @author - Danil Zhyliaiev
 */
public final class Messages {
    /** Message used before shut down program. Used to provide help to user. */
    public static final String HELP_MESSAGE = "How to use:\n"
            + "Fill in envelopes sizes. Program will calculate if first "
            + "envelope fits in second and vice versa.\n"
            + "- Only positive numbers(including both floating-point numbers "
            + "and integers) are allowed as envelope sidelength;\n"
            + "- Both dot('.') and comma(',') can be used as a decimal mark;\n"
            + "- To continue compare another pair of envelopes answer "
            + "'y' or 'yes'(case/spaces/tabs insensitive);\n"
            + "To view this help run program without arguments.\n";
    /** Message to be displayed upon proceed request. */
    public static final String REQUEST_TO_PROCEED = "Do you want to proceed?";
    /** Message to be displayed upon first side request. */
    public static final String REQUEST_FIRST_SIDE = "Enter the FIRST envelope sides:";
    /** Message to be displayed upon second side request. */
    public static final String REQUEST_SECOND_SIDE = "Enter the SECOND envelope sides:";
    /** Message to be displayed when input value is negative. */
    public static final String NEGATIVE_INPUT_ERROR = "Value cannot be negative/zero. "
            + "Enter valid value:";
    /** Message to be displayed when input value cannot be parsed to a number. */
    public static final String WRONG_NUMBER_FORMAT_ERROR = "Wrong number format. Only positive"
            + " numbers are accepted. Enter valid value:";

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private Messages() { }
}
