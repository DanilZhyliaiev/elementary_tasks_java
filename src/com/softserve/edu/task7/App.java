package com.softserve.edu.task7;

import com.softserve.edu.task7.input.InputReader;

import java.io.IOException;

/**
 * This app reads arguments and print all natural numbers which meet
 * condition (i * i) < n.
 * Where:   i - natural number;
 *          n - given argument as int.
 * @author Danil Zhyliaiev
 *
 */
public final class App {
    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private App() { }

    /**
     * Task7 entry point.
     * @param args
     *      Parameters From Command Line.
     * @throws IOException .
     */
    public static void main(final String[] args) throws IOException {
        InputReader inputReader = new InputReader();

        long limit = inputReader.parseLongNumber(args);


        try {
            NumberUtils.printNaturalNumbers(limit);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }
    }
}
