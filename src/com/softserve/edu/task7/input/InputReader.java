package com.softserve.edu.task7.input;

import com.softserve.edu.task7.Messages;

/**
 * Class to get a number from given input(arguments).
 * @author Danil Zhyliaiev
 */

public class InputReader {
    /**
     * Verify given arguments to meet app requirements.
     * @param args  a {@code String[]} containing Command Line arguments
     *              to be verified.
     */
    private void verifyArgs(final String[] args) {
        if (args.length == 0) {
            System.out.println(Messages.HELP_MESSAGE);

            System.exit(0);
        } else if (args.length != 1) {
            System.out.println(Messages.ARGUMENTS_NUMBER_ERROR);
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }
    }

    /**
     * Try to parse long from args[].
     * If number format is incorrect - print message and exit.
     * @param args  CL arguments.
     * @return      given arguments as {@code long}.
     */
    public final long parseLongNumber(final String[] args) {
        verifyArgs(args);
        long number = 0;
        try {
            number = Long.parseLong(args[0]);
        } catch (NumberFormatException e) {
            System.out.println(Messages.WRONG_NUMBER_FORMAT);
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }
        return number;
    }
}
