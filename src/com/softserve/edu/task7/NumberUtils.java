package com.softserve.edu.task7;

/**
 * This class delivers method to print all natural numbers
 * up to specified limit.
 * @author Danil Zhyliaiev
 *
 */
public final class NumberUtils {
    /**
     * Max natural number according to {@code (input <= Integer.MAX_VALUE)}.
     */
    private static final long MAX_VALUE = (long) Math.sqrt(Long.MAX_VALUE);

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private NumberUtils() { }

    /**
     * Prints natural number series from 1 to (i * i) < {@code number}.
     * @param number    positive {@code int} upper limit.
     */
    public static void printNaturalNumbers(final long number) {
        if (number < 0) {
            throw new IllegalArgumentException(Messages.NEGATIVE_NUMBER_ERROR);
        }

        System.out.println("A series of natural numbers where [(i * i) < "
                + number + "]:");

        if (number == 0) {
            System.out.println("NONE");
            return;
        }

        for (long i = 0; (i * i) < number; i++) {
            if (i > MAX_VALUE) {
                break;
            }

            if (i == 0) {
                System.out.print(i);
            } else {
                System.out.print(", " + i);
            }
        }
    }
}
