package com.softserve.edu.task7;

/**
 * Class-storage for default application messages.
 * @author Danil Zhyliaiev
 */
public final class Messages {
    /** Message used before shut down the program. Used to provide help to user. */
    public static final String HELP_MESSAGE = "How to use:\n"
            + "Program prints natural number series from 0 to i, where i meets condition: (i * i) < n.\n"
            + "Run program with n as an argument.\n"
            + "- Only positive integers are allowed.\n"
            + "- Current maximum input value: 2^63-1 (" + Long.MAX_VALUE + ").\n"
            + "- To view this help run program without arguments.";
    /** Message used before shut down program. Used to inform user how to get help. */
    public static final String HOW_TO_GET_HELP = "Run program with no arguments "
            + "to get a detailed guide.";
    /** Message to display if unsupported number of arguments were given. */
    public static final String WRONG_NUMBER_FORMAT = "Wrong number format. "
            + "Only positive integers are accepted.";
    /** Negative number error message. */
    public static final String NEGATIVE_NUMBER_ERROR = "Number is negative. "
            + "Only positive integers are accepted.";
    /** Wrong number of arguments error message. */
    public static final String ARGUMENTS_NUMBER_ERROR = "Wrong number of arguments.";

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private Messages() { }
}
