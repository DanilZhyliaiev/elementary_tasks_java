package com.softserve.edu.task5.numerals.convertor.lang.ru;

import com.softserve.edu.task5.numerals.convertor.Constants;
import com.softserve.edu.task5.numerals.convertor.lang.InWordsConverter;

/**
 * Class to convert number in words according to Russian spelling.
 * @author Danil Zhyliaiev
 */
public class RussianInWordsConverter implements InWordsConverter {
    /** Current work range upper limit(exclusive). */
    private static final int MAX_VALUE = 1000000000;
    /** Current work range lower limit(exclusive). */
    private static final int MIN_VALUE = -1000000000;

    @Override
    /**
     * Convert number to Russian word representation.
     * @param number    {@code int} number to be converted.
     * @return          {@code String} result of conversion.
     */
    public final String convert(final int number)  {
        assertInWorkRange(number);

        StringBuilder numberInWords = new StringBuilder();
        if (number == 0) {
            numberInWords.append(RuNumericConstants.ZERO);
        } else if (number < 0) {
            numberInWords.append(RuNumericConstants.MINUS)
                    .append(" ")
                    .append(convertMillion(-number));
        } else {
            numberInWords.append(convertMillion(number));
        }
        return numberInWords.toString().trim().replaceAll(" +", " ");
    }

    /**
     * Verify that given number is supported.
     * @param number    {@code int} to check.
     */
    private void assertInWorkRange(final int number) {
        if ((number <= (MIN_VALUE) || (number >= MAX_VALUE))) {
            throw new IllegalArgumentException("Number is out of work range. "
                    + "( [-999 999 999 , 999 999 999] )");
        }
    }

    /**
     * Convert number(millions or less) to Russian word representation.
     * @param number    {@code int} number to be converted.
     * @return          {@code String} result of conversion.
     */
    private String convertMillion(final int number) {
        // Checks if number is 0 or less than a million.
        if (number == 0) {
            return "";
        } else if (number < Constants.ONE_MILLION) {
            return convertThousand(number);
        }

        // Digit of millions part
        int millionsValue = number / Constants.ONE_MILLION;
        // All but millions digits
        int thousandsValue = number % Constants.ONE_MILLION;
        // Millions part last digit. Needed to check special russian spelling
        int millionsLastDigit = millionsValue % Constants.ONE_DOZEN;

        StringBuilder result = new StringBuilder();

        /*
         * Process millions part according to russian spelling.
         * Case 1: has 1 as last digit.         "миллион"
         * Case 2: has 2-4 as last digit.       "миллиона"
         * Case 3: has 5-9 as last digit.       "миллионов"
         * Case 4: has 11-19 as last 2 digits.  "миллионов"
         */
        if (((millionsValue % Constants.ONE_HUNDRED) < (Constants.ONE_DOZEN / 2))
                || ((millionsValue % Constants.ONE_HUNDRED) > (Constants.ONE_DOZEN * 2))) {
            if (millionsLastDigit == 1) {                            // Case 1
                millionsValue -= millionsLastDigit;
                result.append(convertHundred(millionsValue))
                        .append(" ")
                        .append(convertUnit(millionsLastDigit))
                        .append(" ")
                        .append(RuNumericConstants.MILLIONS_BASE);
            } else if ((millionsLastDigit) > 1                       // Case 2
                    && (millionsLastDigit < (Constants.ONE_DOZEN / 2))) {
                millionsValue -= millionsLastDigit;
                result.append(convertHundred(millionsValue))
                        .append(" ")
                        .append(convertUnit(millionsLastDigit))
                        .append(" ")
                        .append(RuNumericConstants.MILLIONS_BASE)
                        .append("a");
            } else {                                                // Case 3
                result.append(convertHundred(millionsValue))
                        .append(" ")
                        .append(RuNumericConstants.MILLIONS_BASE)
                        .append("ов");
            }
        } else {                                                    // Case 4
            result.append(convertHundred(millionsValue))
                    .append(" ")
                    .append(RuNumericConstants.MILLIONS_BASE)
                    .append("ов");
        }

        return result.append(" ").append(convertThousand(thousandsValue)).toString();
    }

    /**
     * Convert number(thousands or less) to Russian word representation.
     * @param number    {@code int} number to be converted.
     * @return          {@code String} result of conversion.
     */
    private String convertThousand(final int number) {
        // Checks if number is 0 or less than a thousand.
        if (number == 0) {
            return "";
        } else if (number < Constants.ONE_THOUSAND) {
            return convertHundred(number);
        }

        // Digit of thousands part.
        int thousandsValue = number / Constants.ONE_THOUSAND;
        // All other digits.
        int hundredsValue = number % Constants.ONE_THOUSAND;
        // Thousands part last digit. Needed to check special russian spelling
        int thousandLastDigit = thousandsValue % Constants.ONE_DOZEN;

        StringBuilder result = new StringBuilder();

        /*
         * Process thousands part according to russian spelling.
         * Case 1: has 1 as last digit.         "тысяча"
         * Case 2: has 2-4 as last digit.       "тысячи"
         * Case 3: has 5-9 as last digit.       "тысяч"
         * Case 4: has 11-19 as last 2 digits.  "тысяч"
         */
        if (((thousandsValue % Constants.ONE_HUNDRED) < (Constants.ONE_DOZEN / 2))
                || ((thousandsValue % Constants.ONE_HUNDRED) > (Constants.ONE_DOZEN * 2))) {
            if (thousandLastDigit == 1) {                           // Case 1
                thousandsValue -= thousandLastDigit;
                result.append(convertHundred(thousandsValue))
                        .append(" ")
                        .append(convertUnit(thousandLastDigit, true))
                        .append(" ")
                        .append(RuNumericConstants.THOUSANDS_BASE)
                        .append("а");
            } else if ((thousandLastDigit) > 1                      // Case 2
                    && (thousandLastDigit < (Constants.ONE_DOZEN / 2))) {
                thousandsValue -= thousandLastDigit;
                result.append(convertHundred(thousandsValue))
                        .append(" ")
                        .append(convertUnit(thousandLastDigit, true))
                        .append(" ")
                        .append(RuNumericConstants.THOUSANDS_BASE)
                        .append("и");
            } else {                                                // Case 3
                result.append(convertHundred(thousandsValue))
                        .append(" ")
                        .append(RuNumericConstants.THOUSANDS_BASE);
            }
        } else {                                                    // Case 4
            result.append(convertHundred(thousandsValue))
                    .append(" ")
                    .append(RuNumericConstants.THOUSANDS_BASE);
        }

        return result.append(" ").append(convertHundred(hundredsValue)).toString();
    }

    /**
     * Convert number(hundreds or less) to Russian word representation.
     * @param number    {@code int} number to be converted.
     * @return          {@code String} result of conversion.
     */
    private String convertHundred(final int number) {
        // Checks if number is 0 or less than a hundred.
        if (number == 0) {
            return "";
        } else if (number < Constants.ONE_HUNDRED) {
            return convertDozen(number);
        }

        String[] hundreds = RuNumericConstants.HUNDREDS;
        int hundred = number / Constants.ONE_HUNDRED;
        int dozens = number % Constants.ONE_HUNDRED;

        return hundreds[hundred - 1] + " " + convertDozen(dozens);
    }

    /**
     * Convert number(dozens or less) to Russian word representation.
     * @param number    {@code int} number to be converted.
     * @return          {@code String} result of conversion.
     */
    private String convertDozen(final int number) {
        // Checks if number is 0 or less than a dozen.
        if (number == 0) {
            return "";
        } else if (number < Constants.ONE_DOZEN) {
            return convertUnit(number);
        }

        String[] teens = RuNumericConstants.TEENS;
        String[] dozens = RuNumericConstants.DOZENS;

        // Checks if number is 'teen' or not.
        if (number < (Constants.ONE_DOZEN * 2)) {
            return teens[number - Constants.ONE_DOZEN];
        } else {
            int dozen = number / Constants.ONE_DOZEN;
            int unit = number % Constants.ONE_DOZEN;
            return dozens[dozen - 2] + " " + convertUnit(unit);
        }
    }

    /**
     * Convert number(units) to Russian word representation.
     * @param number    {@code int} number to be converted.
     * @return          {@code String} result of conversion.
     */
    private String convertUnit(final int number) {
        return convertUnit(number, false);
    }

    /**
     * Convert number(units) to Russian word representation.
     * @param number        {@code int} numbr5e4    q2er to be converted.
     * @param isThousand    {@code boolean} shows if number is
     *                      a part of thousands.
     * @return              {@code String} result of conversion.
     */
    private String convertUnit(final int number, final boolean isThousand) {
        if (number == 0) {
            return "";
        }

        // Choose spelling according to thousand/not-thousand flag.
        if (isThousand) {
            String[] units = RuNumericConstants.THOUSAND_UNITS;
            return units[number - 1];
        } else {
            String[] units = RuNumericConstants.UNITS;
            return units[number - 1];
        }
    }
}
