package com.softserve.edu.task5.numerals.convertor.lang;

import com.softserve.edu.task5.numerals.convertor.lang.en.EnglishInWordsConverter;
import com.softserve.edu.task5.numerals.convertor.lang.ru.RussianInWordsConverter;

/**
 * Enumeration of languages that are currently supported.
 * @author Danil Zhyliaiev
 */
public enum SupportedLanguage {
    /** Russian spelling. */
    RUSSIAN(new RussianInWordsConverter()),
    /** English spelling. */
    ENGLISH(new EnglishInWordsConverter());

    /** language-dependant converter. */
    private InWordsConverter converter;

    /**
     * Describes enum initialization format.
     * @param converter     converter to be assigned to chosen language.
     */
    SupportedLanguage(final InWordsConverter converter) {
        this.converter = converter;
    }

    /**
     * Gets converter associated with selected language.
     * @return  {@code InWordsConverter} selected language initialized with.
     */
    public InWordsConverter getConverter() {
        return converter;
    }

}
