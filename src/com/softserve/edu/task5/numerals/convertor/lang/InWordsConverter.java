package com.softserve.edu.task5.numerals.convertor.lang;

/**
 * General number-in-words converter interface.
 * The only method needed to be implemented is convert(),
 * which returns {@code String} and has single {@code int} as parameter.
 * @author Danil Zhyliaiev
 */
public interface InWordsConverter {
    /**
     * Converts given number to string.
     * @param number    number to convert.
     * @return          number inWords representation.
     */
    String convert(int number);
}
