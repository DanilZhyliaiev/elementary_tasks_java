package com.softserve.edu.task5.numerals.convertor.lang.en;

/**
 * Enumeration of numerals in English.
 * @author Danil Zhyliaiev
 */
public final class EnNumericConstants {
    /** Russian 1-9 spelling. */
    public static final String[] UNITS = {"one", "two", "three", "four",
            "five", "six", "seven", "eight", "nine"};
    /** Russian 10-19 spelling. */
    public static final String[] TEENS = {"ten", "eleven", "twelve", "thirteen",
            "fourteen", "fifteen", "sixteen",
            "seventeen", "eighteen", "nineteen"};
    /** Russian 20-100 dozens spelling. */
    public static final String[] DOZENS = {"twenty", "thirty", "forty", "fifty",
            "sixty", "seventy", "eighty", "ninety"};
    /** Russian 100-900 hundreds spelling. */
    public static final String HUNDREDS_BASE = "hundred";
    /** Russian thousands spelling. */
    public static final String THOUSANDS_BASE = "thousand";
    /** Russian millions spelling. */
    public static final String MILLIONS_BASE = "million";
    /** Russian zero spelling. */
    public static final String ZERO = "zero";
    /** Russian minus spelling. */
    public static final String MINUS = "minus";

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private EnNumericConstants() { }
}
