package com.softserve.edu.task5.numerals.convertor.lang.ru;

/**
 * Enumeration of numerals in Russian.
 * @author Danil Zhyliaiev
 */
public final class RuNumericConstants {
    /** Russian 1-9 spelling. */
    public static final String[] UNITS = {"один", "два", "три", "четыре",
            "пять", "шесть", "семь", "восемь", "девять"};
    /** Russian 1-9 spelling used with thousands. */
    public static final String[] THOUSAND_UNITS = {"одна", "две", "три", "четыре",
            "пять", "шесть", "семь", "восемь", "девять"};
    /** Russian 10-19 spelling. */
    public static final String[] TEENS = {"десять", "одиннадцать", "двенадцать",
            "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать",
            "семнадцать", "восемнадцать", "девятнадцать"};
    /** Russian 20-100 dozens spelling. */
    public static final String[] DOZENS = {"двадцать", "тридцать", "сорок", "пятьдесят",
            "шеcтьдесят", "семьдесять", "восемьдесят", "девяносто"};
    /** Russian 100-900 hundreds spelling. */
    public static final String[] HUNDREDS = {"сто", "двести", "триста", "четыреста",
            "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
    /** Russian thousands spelling. */
    public static final String THOUSANDS_BASE = "тысяч";
    /** Russian millions spelling. */
    public static final String MILLIONS_BASE = "миллион";
    /** Russian zero spelling. */
    public static final String ZERO = "ноль";
    /** Russian minus spelling. */
    public static final String MINUS = "минус";

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private RuNumericConstants() { }
}
