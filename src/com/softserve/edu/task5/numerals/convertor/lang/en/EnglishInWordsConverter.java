package com.softserve.edu.task5.numerals.convertor.lang.en;

import com.softserve.edu.task5.numerals.convertor.Constants;
import com.softserve.edu.task5.numerals.convertor.lang.InWordsConverter;

/**
 * Class to convert number in words accroding to English spelling.
 * @author Danil Zhyliaiev
 */
public class EnglishInWordsConverter implements InWordsConverter {
    /** Current work range upper limit(exclusive). */
    private static final int MAX_VALUE = 1000000000;
    /** Current work range lower limit(exclusive). */
    private static final int MIN_VALUE = -1000000000;

    @Override
    /**
     * Convert number to English word representation.
     * @param number    {@code int} number to be converted.
     * @return          {@code String} result of conversion.
     */
    public final String convert(final int number)  {
        assertInWorkRange(number);

        StringBuilder numberInWords = new StringBuilder();
        if (number == 0) {
            numberInWords.append(EnNumericConstants.ZERO);
        } else if (number < 0) {
            numberInWords.append(EnNumericConstants.MINUS)
                    .append(" ")
                    .append(convertMillion(-number));
        } else {
            numberInWords.append(convertMillion(number));
        }
        return numberInWords.toString().trim().replaceAll(" +", " ");
    }

    /**
     * Verify that given number is supported.
     * @param number    {@code int} to check.
     */
    private void assertInWorkRange(final int number) {
        if ((number <= (MIN_VALUE) || (number >= MAX_VALUE))) {
            throw new IllegalArgumentException("Number is out of work range. "
                    + "( [-999 999 999 , 999 999 999] )");
        }
    }

    /**
     * Convert number(millions or less) to English word representation.
     * @param number    {@code int} number to be converted.
     * @return          {@code String} result of conversion.
     */
    private String convertMillion(final int number) {
        // Checks if number is 0 or less than a million.
        if (number == 0) {
            return "";
        } else if (number < Constants.ONE_MILLION) {
            return convertThousand(number);
        }

        // Digit of millions part
        int millionsValue = number / Constants.ONE_MILLION;
        // All but millions digits
        int thousandsValue = number % Constants.ONE_MILLION;

        return convertHundred(millionsValue) + " "
                + EnNumericConstants.MILLIONS_BASE + " "
                + convertThousand(thousandsValue);
    }

    /**
     * Convert number(thousands or less) to English word representation.
     * @param number    {@code int} number to be converted.
     * @return          {@code String} result of conversion.
     */
    private String convertThousand(final int number) {
        // Checks if number is 0 or less than a thousand.
        if (number == 0) {
            return "";
        } else if (number < Constants.ONE_THOUSAND) {
            return convertHundred(number);
        }

        // Digit of thousands part.
        int thousandsValue = number / Constants.ONE_THOUSAND;
        // All other digits.
        int hundredsValue = number % Constants.ONE_THOUSAND;

        return convertHundred(thousandsValue) + " "
                + EnNumericConstants.THOUSANDS_BASE + " "
                + convertHundred(hundredsValue);
    }

    /**
     * Convert number(hundreds or less) to English word representation.
     * @param number    {@code int} number to be converted.
     * @return          {@code String} result of conversion.
     */
    private String convertHundred(final int number) {
        // Checks if number is 0 or less than a hundred.
        if (number == 0) {
            return "";
        } else if (number < Constants.ONE_HUNDRED) {
            return convertDozen(number);
        }

        int hundred = number / Constants.ONE_HUNDRED;
        int dozens = number % Constants.ONE_HUNDRED;

        return convertUnit(hundred) + " " + EnNumericConstants.HUNDREDS_BASE
                + " " + convertDozen(dozens);
    }

    /**
     * Convert number(dozens or less) to English word representation.
     * @param number    {@code int} number to be converted.
     * @return          {@code String} result of conversion.
     */
    private String convertDozen(final int number) {
        // Checks if number is 0 or less than a dozen.
        if (number == 0) {
            return "";
        } else if (number < Constants.ONE_DOZEN) {
            return convertUnit(number);
        }

        String[] teens = EnNumericConstants.TEENS;
        String[] dozens = EnNumericConstants.DOZENS;

        // Checks if number is 'teen' or not.
        if (number < (Constants.ONE_DOZEN * 2)) {
            return teens[number - Constants.ONE_DOZEN];
        } else {
            int dozen = number / Constants.ONE_DOZEN;
            int unit = number % Constants.ONE_DOZEN;
            return dozens[dozen - 2] + "-" + convertUnit(unit);
        }
    }

    /**
     * Convert number(units) to English word representation.
     * @param number        {@code int} number to be converted.
     * @return              {@code String} result of conversion.
     */
    private String convertUnit(final int number) {
        if (number == 0) {
            return "";
        }

        String[] units = EnNumericConstants.UNITS;
        return units[number - 1];
    }
}
