package com.softserve.edu.task5.numerals.convertor;

import com.softserve.edu.task5.numerals.convertor.lang.SupportedLanguage;

/**
 * Provides number-in-words conversion using given language.
 * @author Danil Zhyliaiev
 */
public class NumberInWords {
    /**
     * Default inWords convertor. Uses Russian as default language.
     * @param number    {@code int} to convert in words.
     * @return          converted number as {@code String}.
     */
    public final String convertInWords(final int number) {
        return convertInWords(number, SupportedLanguage.RUSSIAN);
    }

    /**
     * inWords convertor. Converts given number to given language.
     * @param number        {@code int} to convert in words.
     * @param language      desired language.
     * @return              converted number as {@code String}.
     */
    public final String convertInWords(final int number, final SupportedLanguage language) {
        return language.getConverter().convert(number);
    }
}
