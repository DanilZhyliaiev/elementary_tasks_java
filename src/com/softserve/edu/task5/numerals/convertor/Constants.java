package com.softserve.edu.task5.numerals.convertor;

/**
 * Constants used in number-in-word conversions.
 * @author Danil Zhyliaiev
 */
public final class Constants {
    /** Value of 1 million. */
    public static final int ONE_MILLION = 1000000;
    /** Value of 1 thousand. */
    public static final int ONE_THOUSAND = 1000;
    /** Value of 1 hundred. */
    public static final int ONE_HUNDRED = 100;
    /** Value of 1 dozen. */
    public static final int ONE_DOZEN = 10;

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private Constants() { }
}
