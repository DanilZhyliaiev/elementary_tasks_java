package com.softserve.edu.task5.input;

import com.softserve.edu.task5.Messages;

/**
 * Class to get a number from given input(arguments).
 * @author Danil Zhyliaiev
 */
public class InputReader {

    /**
     * Verify given arguments to meet app requirements. Prints user guide
     * if no arguments were given.
     * @param args  a {@code String[]} containing Command Line arguments
     *              to be verified.
     */
    private void verifyArgs(final String[] args) {
        if (args.length == 0) {
            System.out.println(Messages.HELP_MESSAGE);

            System.exit(0);
        } else if (args.length != 1) {
            System.out.println(Messages.WRONG_ARGS_NUMBER_MESSAGE);
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }
    }

    /**
     * Try to parse integer from args[].
     * If number format is incorrect - print message and exit.
     * @param args  CL arguments.
     * @return      given arguments as {@code int}.
     */
    public final int parseIntNumber(final String[] args) {
        verifyArgs(args);

        int number = 0;
        try {
            number = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            System.out.println("Wrong number format");
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }

        return number;
    }
}
