package com.softserve.edu.task5;

import com.softserve.edu.task5.input.InputReader;
import com.softserve.edu.task5.numerals.convertor.NumberInWords;
import com.softserve.edu.task5.numerals.convertor.lang.SupportedLanguage;

/**
 * This app convert integer to its representation in Russian.
 * @author Danil Zhyliaiev
 *
 */
public final class App {

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private App() { }

    /**
     * Task5 entry point.
     * @param args
     *      Parameters From Command Line.
     */
    public static void main(final String[] args) {
        InputReader inputReader = new InputReader();

        int number = inputReader.parseIntNumber(args);

        NumberInWords inWords = new NumberInWords();

        try {
            System.out.println(inWords.convertInWords(number, SupportedLanguage.ENGLISH));
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }
    }
}
