package com.softserve.edu.task5;

/**
 * Class-storage for default application messages.
 * @author Danil Zhyliaiev
 */
public final class Messages {
    /** Message used before shut down the program. Used to provide help to user. */
    public static final String HELP_MESSAGE = "How to use:\n"
            + "To convert an integer to its Russian word representation\n"
            + "run program with the number(without spaces) as an argument.\n"
            + "- Only integers are allowed.\n"
            + "- Current application work range: [-999 999 999 , 999 999 999] (without spaces).\n"
            + "- To view this help run program without arguments.";
    /** Message used before shut down program. Used to inform user how to get help. */
    public static final String HOW_TO_GET_HELP = "Run program with no arguments "
            + "to get a detailed guide.";
    /** Message used if arguments number is incorrect. */
    public static final String WRONG_ARGS_NUMBER_MESSAGE = "Wrong number of arguments.";

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private Messages() { }
}
