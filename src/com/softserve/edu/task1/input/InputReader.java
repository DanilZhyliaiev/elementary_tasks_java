package com.softserve.edu.task1.input;

import com.softserve.edu.task1.Messages;

/**
 * Reads arguments, verify it and return as array of integers.
 * @author - Danil Zhyliaiev
 */
public class InputReader {
    /**
     * Parses arguments to array of ints.
     * @param args  CL arguments to be verified and parsed.
     * @return      arguments parsed to numbers as {@code int[]}.
     */
    public final int[] getParameters(final String[] args) {
        checkArgsNumber(args);

        int[] params = new int[2];

        try {
            params[0] = Integer.parseInt(args[0]);
            params[1] = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            System.out.println(String.format("Arguments number format "
                    + "is incorrect: '%s %s'.",
                    args[0], args[1]));
            System.out.println("Only positive integers allowed.");
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }

        return params;
    }

    /**
     * Check if args[] length is incorrect. If so - print message and exit.
     * Prints help message if no arguments were given.
     * @param args  a {@code String[]} containing Command Line arguments
     *              to be verified.
     */
    private void checkArgsNumber(final String[] args) {
        if (args.length == 0) {
            System.out.println(Messages.HELP_MESSAGE);

            System.exit(0);
        } else if (args.length != 2) {
            System.out.println("Wrong number of arguments: " + args.length
                    + "\nProgram requires exactly"
                    + " two arguments to work.");
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }
    }
}
