package com.softserve.edu.task1;

/**
* An abstract representation of 2-colored chessboard.
* @author Danil
*
*/
public final class Chessboard {
    /** Chessboard height. */
    private int height;
    /** Chessboard width. */
    private int width;
    /** Chessboard first field-type 'color' represented as character. */
    private char darkField;
    /** Chessboard second field-type 'color' represented as character. */
    private char brightField;

    /**
     * Allocate a {@code Chessboard} object with given parameters
     * and default field colors.
     * @param height        desired chessboard height as {@code int}.
     * @param width         desired chessboard width as {@code int}.
     * @param darkField    Abstract color of the dark fields
     *                      represented by {@code char}.
     * @param brightField  Abstract color of the bright fields
     *                      represented by {@code char}.
     */
    public Chessboard(final int height, final int width,
                      final char darkField, final char brightField) {
        setHeight(height);
        setWidth(width);
        setDarkField(darkField);
        setBrightField(brightField);
    }

    /**
     * Allocate a {@code Chessboard} object with given parameters
     * and default field-type colors.
     * @param height    desired chessboard height as {@code int}.
     * @param width     desired chessboard width as {@code int}.
     */
    public Chessboard(final int height, final int width) {
        this(height, width, '*', ' ');
    }

    /**
     * Gets {@code Chessboard} height.
     * @return      height as {@code int}.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Validate if value is positive and sets chessboard height to given value.
     * @param height    {@code int} to set.
     */
    public void setHeight(final int height) {
        if (isValidValue(height)) {
            this.height = height;
        } else {
            throw new IllegalArgumentException("Height value is negative/zero: "
                    + height + "\nAll arguments should be positive integers.");
        }
    }

    /**
     * Gets {@code Chessboard} width.
     * @return      width as {@code int}.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Validate if value is positive and sets chessboard width to given value.
     * @param width    {@code int} to set.
     */
    public void setWidth(final int width) {
        if (isValidValue(width)) {
            this.width = width;
        } else {
            throw new IllegalArgumentException("Width value is negative/zero: "
                    + width + "\nAll arguments should be positive integers.");
        }
    }

    /**
     * Sets {@code char} representation of dark fields to given value.
     * Validate that bright fields doesn't represented by the same {@code char}.
     * @param darkField    {@code char} to set as dark fields.
     */
    public void setDarkField(final char darkField) {
        if (this.brightField != darkField) {
            this.darkField = darkField;
        } else {
            throw new IllegalArgumentException(
                    "Chessboard cannot be paint in two equal colors.");
        }
    }

    /**
     * Sets {@code char} representation of bright fields to given value.
     * Validate that bright fields doesn't represented by the same {@code char}.
     * @param brightField      {@code char} to set as bright fields.
     */
    public void setBrightField(final char brightField) {
        if (this.darkField != brightField) {
            this.brightField = brightField;
        } else {
            throw new IllegalArgumentException(
                    "Chessboard cannot be paint in two equal colors.");
        }
    }

    /**
     * Checks if given value is valid chessboard side-length.
     * @param sidelength    {@code int} to validate.
     * @return              {@code true} if value is valid.
     *                      {@code false} if value is invalid.
     */
    private boolean isValidValue(final int sidelength) {
        return sidelength > 0;
    }

    @Override
    public String toString() {
        StringBuilder chessboard = new StringBuilder(height * width + height);

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (i % 2 != 0) {
                    if (j % 2 == 0) {
                        chessboard.append(darkField);
                    } else {
                        chessboard.append(brightField);
                    }
                } else {
                    if (j % 2 == 0) {
                        chessboard.append(brightField);
                    } else {
                        chessboard.append(darkField);
                    }
                }
            }
            chessboard.append("\n");
        }

        return chessboard.toString();
    }
}
