package com.softserve.edu.task1;

import com.softserve.edu.task1.input.InputReader;

/**
 * This app prints chessboard with given parameters.
 * @author Danil Zhyliaiev
 *
 */
public final class App {
    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private App() { }

    /**
     * Task1 entry point.
     * @param args
     *      Parameters From Command Line
     */
    public static void main(final String[] args) {
        int[] parameters = new InputReader().getParameters(args);

        int height = parameters[0];
        int width = parameters[1];

        try {
            Chessboard board = new Chessboard(height, width);
            System.out.println(board);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            System.out.println(Messages.HOW_TO_GET_HELP);

            System.exit(-1);
        }
    }
}
