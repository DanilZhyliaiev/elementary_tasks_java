package com.softserve.edu.task1;

/**
 * Class-container for application default messages.
 * @author - Danil Zhyliaiev
 */
public final class Messages {
    /** Message used before shut down program. Used to provide help to user. */
    public static final String HELP_MESSAGE = "How to use:\n"
            + "Run App with two positive integers as arguments using next "
            + "format:\n"
            + "<chessboard height> <chessboard width>\n"
            + "2-colored chessboard will be printed using first integer"
            + " as height and second integer as width\n"
            + "- Chessboard dark and bright fields represented by "
            + "'*' and ' ' respectively."
            + "- Only positive integers are allowed as chessboard "
            + "side-length;\n"
            + "To view this help run program without arguments.";
    /** Message used before shut down program.
     *  Used to inform user how to get help. */
    public static final String HOW_TO_GET_HELP = "Run program with"
            + " no arguments to get a detailed guide.";

    /**
     *  Suppresses default constructor, ensuring non-instantiability.
     */
    private Messages() { }
}
